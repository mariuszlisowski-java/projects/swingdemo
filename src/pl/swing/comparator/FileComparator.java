package pl.swing.comparator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

public class FileComparator implements ActionListener {
    JTextField fileFirstField, fileSecondField;
    JLabel fileFirstLabel, fileSecondLabel;
    JLabel resultLabel;
    JButton compareButton;


    FileComparator() {
        JFrame frame = new JFrame("Text files comparator");
        frame.setLayout(new FlowLayout(FlowLayout.LEFT));
        frame.setSize(350, 150);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);

        fileFirstLabel = new JLabel("First file:     ");
        fileSecondLabel = new JLabel("Second file: ");

        fileFirstField = new JTextField(20);
        fileSecondField = new JTextField(20);

        fileFirstField.setActionCommand("firstFilename");
        fileSecondField.setActionCommand("secondFilename");

        compareButton = new JButton("Compare");
        resultLabel = new JLabel("");

        compareButton.addActionListener(this);
        fileFirstField.addActionListener(this);
        fileSecondField.addActionListener(this);

        frame.add(fileFirstLabel);
        frame.add(fileFirstField);
        frame.add(fileSecondLabel);
        frame.add(fileSecondField);
        frame.add(compareButton);
        frame.add(resultLabel);
        frame.setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Compare")) {
            System.out.println("comparing...");
            System.out.println(fileFirstField.getText());
            System.out.println(fileSecondField.getText());

            try {
                if (compareFiles(fileFirstField.getText(), fileSecondField.getText())) {
                    resultLabel.setText("Files are the same.");
                } else {
                    resultLabel.setText("Files are different.");
                }
            } catch (IOException ioe) {
                resultLabel.setText("File read error!");
            }
        }
        if (e.getActionCommand().equals("firstFilename")) {
            System.out.println("first file reading...");
        }
        if (e.getActionCommand().equals("secondFilename")) {
            System.out.println("second file readig...");
        }
    }

    private boolean compareFiles(String firstFilename, String secondFilename) throws IOException {
        List<String> firstFile = Files.readAllLines(Paths.get(firstFilename));
        List<String> secondFile = Files.readAllLines(Paths.get(secondFilename));

        if (firstFile.size() != secondFile.size()) {
            return false;
        }

        for (Iterator<String> it1 = firstFile.iterator(), it2 = secondFile.iterator();
             it1.hasNext() && it2.hasNext();)
        {
            if (!it1.next().equals(it2.next())) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(FileComparator::new);
    }
}
