package pl.swing.demo.anonymous;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonAnonym {

    ButtonAnonym() {
        JFrame frame = new JFrame();
        frame.setSize(200, 100);
        frame.setLayout(new FlowLayout(FlowLayout.CENTER));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton button = new JButton("Click me");
        JLabel label = new JLabel();

        // anonymous function (ActionListener interface implementation)
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                label.setText("Button was clicked");

                Timer timer = new Timer(500, new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        label.setText("");
                    }
                });
                timer.setRepeats(false);
                timer.start();
            }
        });

        frame.add(button);
        frame.add(label);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(ButtonAnonym::new);
    }
}
