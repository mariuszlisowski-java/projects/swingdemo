package pl.swing.demo.implement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TextFieldDemo implements ActionListener {
    JTextField textField;
    JLabel promptLabel;

    TextFieldDemo() {
        JFrame frame = new JFrame("Reverse me");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());
        frame.setSize(300, 100);

        promptLabel = new JLabel("Entry to reverse: ");

        textField = new JTextField("enter anything", 10);
        textField.setActionCommand("enter");

        JButton reverseButton = new JButton("Reverse");
        JLabel infoLabel = new JLabel("Press Enter or click Reverse");

        textField.addActionListener(this);
        reverseButton.addActionListener(this);


        frame.add(promptLabel);
        frame.add(textField);
        frame.add(reverseButton);
        frame.add(infoLabel);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("enter") ||
            e.getActionCommand().equals("Reverse")) {
            textField.setText(reverse(textField.getText()));
        }
        if (e.getActionCommand().equals("Reverse")) {

        }
    }

    private String reverse(String text) {
        StringBuilder reversed = new StringBuilder();
        for (int i = text.length() - 1; i >= 0; --i) {
            reversed.append(text.charAt(i));
        }

        return reversed.toString();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TextFieldDemo();
            }
        });
    }
}
