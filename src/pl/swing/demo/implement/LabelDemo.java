package pl.swing.demo.implement;

import javax.swing.*;

public class LabelDemo {
    JFrame frame = new JFrame("Demo");

    LabelDemo() {
        frame.setSize(200,150);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel label = new JLabel("Swing's here");
        label.setHorizontalAlignment(JLabel.CENTER);
        frame.add(label);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new LabelDemo();
            }
        });
    }
}
