package pl.swing.demo.implement;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.stream.IntStream;

public class ListDemo implements ListSelectionListener {
    JList<Integer> yearsList;
    JScrollPane scrollPane;
    JLabel promptLabel;
    JLabel chooseLabel;

    Integer[] years = IntStream.rangeClosed(1996, 2021)
                               .boxed()
                               .toArray(Integer[]::new);

    ListDemo() {
        JFrame frame = new JFrame("Years selector");
        frame.setLayout(new FlowLayout());
        frame.setSize(200, 250);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);

        yearsList = new JList<Integer>(years);
        yearsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // enable scrolling
        scrollPane = new JScrollPane(yearsList);
        scrollPane.setPreferredSize(new Dimension(130, 150));


        JPanel panel = new JPanel(new BorderLayout());

        promptLabel = new JLabel("Select year");
        chooseLabel = new JLabel();

        yearsList.addListSelectionListener(this);

        panel.add(promptLabel, BorderLayout.NORTH);
        panel.add(chooseLabel, BorderLayout.SOUTH);

        frame.add(scrollPane);
        frame.add(panel);

        frame.setVisible(true);
    }


    @Override
    public void valueChanged(ListSelectionEvent e) {
        int index = yearsList.getSelectedIndex();

        if (index != -1) {
            chooseLabel.setText(String.valueOf(years[index]));
        }

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(ListDemo::new);
    }
}
