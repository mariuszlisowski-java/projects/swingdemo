package pl.swing.demo.implement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonDemo implements ActionListener  {
    JLabel labelChoose;

    ButtonDemo() {
        JFrame frame = new JFrame("Buttons");
        frame.setLayout(new FlowLayout());
        frame.setSize(300,100);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel labelPrompt = new JLabel("Your choice: ");
        frame.add(labelPrompt);

        JButton upButton = new JButton("Up");
        JButton downButton = new JButton("Down");

        upButton.addActionListener(this);
        downButton.addActionListener(this);

        frame.add(upButton);
        frame.add(downButton);

        labelChoose = new JLabel();
        frame.add(labelChoose);

        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Up")) {
            labelChoose.setText("Up chosen");
        }
        if (e.getActionCommand().equals("Down")) {
            labelChoose.setText("Down chosen");
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ButtonDemo();
            }
        });
    }

}
