package pl.swing.demo.implement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class CheckBoxDemo implements ItemListener {
    JCheckBox alphaBox;
    JCheckBox betaBox;
    JCheckBox gammaBox;
    JLabel resultLabel;
    JLabel currentLabel;
    JLabel currentLabelText;

    CheckBoxDemo() {
        JFrame frame = new JFrame("Checker");
        frame.setSize(250, 150);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout(FlowLayout.CENTER));
        frame.setResizable(false);

        alphaBox = new JCheckBox("Alpha");
        betaBox = new JCheckBox("Beta");
        gammaBox = new JCheckBox("Gamma");

        alphaBox.addItemListener(this);
        betaBox.addItemListener(this);
        gammaBox.addItemListener(this);

        JPanel panelTop = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel panelBottom = new JPanel(new BorderLayout());

        resultLabel = new JLabel("Nothing was selected");

        currentLabelText = new JLabel("Currenty selected:");
        currentLabel = new JLabel();

        panelTop.add(alphaBox);
        panelTop.add(betaBox);
        panelTop.add(gammaBox);

        panelBottom.add(resultLabel, BorderLayout.NORTH);
        panelBottom.add(currentLabelText, BorderLayout.CENTER);
        panelBottom.add(currentLabel, BorderLayout.SOUTH);

        frame.add(panelTop);
        frame.add(panelBottom);

        frame.setVisible(true);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        // first label
        JCheckBox current = (JCheckBox) e.getItem();
        if (current.isSelected()) {
            resultLabel.setText(current.getText() + " was selected");
        }
        if (!current.isSelected()) {
            resultLabel.setText(current.getText() + " was unselected");
        }
        // second label
        StringBuilder stringBuilder = new StringBuilder(" ");
        if (alphaBox.isSelected()) {
            stringBuilder.append("Alpha ");
        }
        if (betaBox.isSelected()) {
            stringBuilder.append("Beta ");
        }
        if (gammaBox.isSelected()) {
            stringBuilder.append("Gamma");
        }
        currentLabel.setText(stringBuilder.toString());
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new CheckBoxDemo();
            }
        });
    }
}
