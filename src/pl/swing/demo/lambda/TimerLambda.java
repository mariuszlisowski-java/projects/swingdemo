package pl.swing.demo.lambda;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.atomic.AtomicInteger;

public class TimerLambda {

    TimerLambda() {
        JFrame frame = new JFrame("Timer");
        frame.setLayout(new FlowLayout(FlowLayout.CENTER));
        frame.setSize(200,200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);

        JPanel panel = new JPanel(new BorderLayout(0, 20));
        JLabel label = new JLabel("0", JLabel.CENTER);

        JButton button = new JButton("Start");
        JButton reset = new JButton("Reset");

        AtomicInteger counter = new AtomicInteger(1);
        Timer timer = new Timer(1000, event -> {
            label.setText(String.valueOf(counter.getAndIncrement()));
        });

        // lambda (ActionListener interface implementation)
        button.addActionListener(event -> {
            if (button.getText().equals("Start")) {
                timer.start();
                button.setText("Stop");
            } else {
                timer.stop();
                button.setText("Start");
            }
        });

        reset.addActionListener(event -> {
            timer.stop();
            counter.set(1);
            label.setText("0");
            button.setText("Start");
        });

        panel.add(button, BorderLayout.NORTH);
        panel.add(label, BorderLayout.CENTER);
        panel.add(reset, BorderLayout.SOUTH);

        frame.add(panel);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(TimerLambda::new);
    }
}
